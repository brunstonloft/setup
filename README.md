# Setup

![screenshot](./assets/screenshot.png)

## Operating System

[macOS Ventura](https://www.apple.com/macos/ventura/)

## Applications

| Name                                                         | Description                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Rectangle](https://rectangleapp.com/)                       | Move and resize windows in macOS using keyboard shortcuts or snap areas. |
| [Little Snitch](https://www.obdev.at/products/littlesnitch/index.html) | Firewall.                                                    |
| [Dozer](https://github.com/Mortennn/Dozer)                   | Hide menubar icons.                                          |
| [AlDente](https://apphousekitchen.com/)                      | Keep your battery fresh (limits battery charging).           |
| [Contexts](https://contexts.co/)                             | Alt-tab, supercharged, for macOS.                            |
| [Meetingbar](https://github.com/leits/MeetingBar)            | Quick links to web meetings.                                 |
| [Clocker](https://apps.apple.com/us/app/clocker/id1056643111?mt=12) | Support multiple timezones with useful time scroller.        |
| [Itsycal](https://www.mowglii.com/itsycal/)                  | Easy month-view (how is this still not part of the macOS default?!) |
| [Jolt of Caffeine](https://apps.apple.com/us/app/jolt-of-caffeine/id1437130425?mt=12) | Prevent screen sleep.                                        |
| [CoconutBattery](https://www.coconut-flavour.com/coconutbattery/) | View battery health.                                         |
| [Mimestream](https://mimestream.com/)                        | Better Mail.app for Gmail.                                   |
| [Typora](https://typora.io/)                                 | A truly minimal Markdown editor.                             |
| [Firefox](https://www.mozilla.org/en-US/firefox/new/)        | Proud user of Gecko since Netscape.                          |
| [Brave](https://brave.com/)                                  | Only because Google Meet works better in Blink.              |

## Terminal

- [iTerm2](https://iterm2.com/)
- [Z shell](https://en.wikipedia.org/wiki/Z_shell)
- [Starship](https://starship.rs/)

## Development Environment

- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)
- [Colima](https://github.com/abiosoft/colima)
- [Visual Studio Code](https://code.visualstudio.com/)
- [homebrew](https://brew.sh/)
- [MacPorts](https://www.macports.org/)
- [neovim](https://ports.macports.org/port/neovim/)
- Python, Go, (and Rust, Lua)

### Utilities

| Name                                                      | Description                                                  |
| --------------------------------------------------------- | ------------------------------------------------------------ |
| [lazydocker](https://github.com/jesseduffield/lazydocker) | The lazier way to manage everything docker.                  |
| [The Fuck](https://github.com/nvbn/thefuck)               | Magnificent app which corrects your previous console command. |
| [ghq](https://github.com/x-motemen/ghq)                   | Manage remote repository clones.                             |
| [z](https://github.com/rupa/z)                            | Jump around (based on 'frecency').                           |

### Visual Studio Code Extensions

| Name                                                         | Description                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Remote Development](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack) | An extension pack that lets you open any folder in a container, on a remote machine, or in WSL and take advantage of VS Code's full feature set. |
| [Git Graph](https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph) | View a Git Graph of your repository, and perform Git actions from the graph. |
| [Gitlab Workflow](https://marketplace.visualstudio.com/items?itemName=fatihacet.gitlab-workflow) | GitLab VSCode integration.                                   |
| [DotENV](https://marketplace.visualstudio.com/items?itemName=mikestead.dotenv) | Support for dotenv file syntax.                              |
| [GraphQL for VSCode](https://marketplace.visualstudio.com/items?itemName=kumar-harsh.graphql-for-vscode) | GraphQL syntax highlighting, linting, auto-complete, and more! |
| [hexdump for VSCode](https://marketplace.visualstudio.com/items?itemName=slevesque.vscode-hexdump) | Display a specified file in hexadecimal.                     |
