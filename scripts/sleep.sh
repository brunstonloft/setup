#!/usr/bin/env bash

if ! pgrep -u $(whoami) -af sshd; then
	gcloud compute instances suspend $(hostname) --zone=ZONE --project=PROJECT
else
	echo "SSH session active; can't sleep"
fi
